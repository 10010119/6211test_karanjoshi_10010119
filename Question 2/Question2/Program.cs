﻿/*------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------Question 2 ------------------------------------------------------*/
/*---------------------------------Learning outcomes 1 & 2 - Data Types and Recurssion------------------------------------*/
/*----------------------------------------------------Marks - Out of 10----------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2
{
/*------------------------------------------------------------------------------------------------------------------------*/
/*-----------Question 2 - Implement a recursive method to find a factorial using hardcoded and user input numbers---------*/
/*------------------------------------------------------Marks - Out of 10-------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
    class Program
    {
        static void Main(string[] args)
        /*Hardcoded and user input passed to the factorial recursive method(Marks out of 5)*/
        {
            const int size = 1000;
            int[] arr = new int[size];
            Stopwatch st = new Stopwatch();

            arr = Populate(arr);                        //Populates the array with random numbers
            Display(arr);

            Console.WriteLine("Please enter a number between 1 and 200 to search for: ");
            int value = Convert.ToInt32(Console.ReadLine());

            st.Start();
            int output2 = RecusiveBinary(arr, value, 0, arr.Length - 1);//Recursive Binary Search
            st.Stop();

            Console.WriteLine("Recursive Binary Time taken to find the value {0}: {1}", output2, st.Elapsed);
            Console.ReadLine();

        }
        public static void Display(int[] arr)
        {
            for (int i = 1; i <= arr.Length; i++)
            {
                Console.Write(arr[i - 1] + " ");
                if (i % 10 == 0)                        //will only display 10 value per line.
                    Console.WriteLine();
            }
            Console.WriteLine();
        }

        /*---Populate Array Method---*/
        public static int[] Populate(int[] arr)
        {
            int min = 0;                                //Lowset possible number
            int max = 200;                              //Highest possible number
            Random randNum = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = randNum.Next(min, max);        //Assign the random number to the i indicies in the array
            }
            return arr;                                 //Return the populated array
        }

        public static int RecusiveBinary(int[] arr, int value, int lower, int upper)
        /*Recursive method (Marks out of 5)*/
        {
            if (lower > upper)
            {
                return -1;
            }
            else
            {
                int mid;
                mid = (int)(upper + lower) / 2;
                if (value < arr[mid])
                {
                    return RecusiveBinary(arr, value, lower, mid - 1);
                }
                else if (value == arr[mid])
                {
                    return mid;
                }
                else
                {
                    return RecusiveBinary(arr, value, mid + 1, upper);
                }
            }
        }

        

        }
    }

/*------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------Question 2 END-------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/

